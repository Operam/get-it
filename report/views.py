from flask import Blueprint, jsonify

api = Blueprint("Report API", __name__, url_prefix='/report')

@api.route('/test', methods=['GET'])
def test_report():
    return jsonify({'message': 'report section'}), 200