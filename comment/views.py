from flask import Blueprint, jsonify

api = Blueprint("Comment API", __name__, url_prefix='/comment')


@api.route('/test', methods=['GET'])
def test_comment():
    return jsonify({'message': 'comment section'}), 200