
class DuplicateReferenceException(Exception):
    def __init__(self, reference):
        self.reference = reference
        super().__init__("The reference {} already exists.".format(reference))