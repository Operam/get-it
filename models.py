from datetime import datetime
from enum import Enum
from config import db
from werkzeug.security import generate_password_hash, check_password_hash  # Import the password hash functions

# from flask_admin.contrib.sqla import ModelView
# from wtforms import SelectField
# from wtforms.validators import DataRequired
# from flask_admin.contrib.sqla import ModelView
# from flask_admin.form import Select2Widget
user_equipment = db.Table(
    'user_equipment',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('equipment_id', db.Integer, db.ForeignKey('equipment.id'), primary_key=True),
    db.Column('active', db.Boolean, nullable=False, default=True),
    db.Column('last_assignment_date', db.DateTime, nullable=False, default=datetime.utcnow)
)
class BaseModel(db.Model):

    __abstract__ = True #abstract class

    def before_save(self,required_attributes=None):
        """
        Method used to verify and perform actions before saving an object.
        Parameters:
            required_attributes(dict): 
                A dictionary specifying the required attributes and their information.
        Raises:
            ValueError: 
                If a required attribute is missing or if an attribute has incorrect type.
        """
        if required_attributes is not None:
            for attribute, attr_info in required_attributes.items():
                attribute_value = getattr(self, attribute)
                if attr_info['required'] and attribute_value is None:
                    raise ValueError("The attribute -{}- is required".format(attribute))
                if not isinstance(attribute_value, attr_info['type']):
                    raise ValueError("The attribute -{}- must be of type {}".format(attribute,attr_info['type'].__name__))

    
class User(BaseModel):
    __tablename__ = 'user'

    class RoleEnum(Enum):
        ADMIN = 'ADMIN'
        CONSULTANT = 'CONSULTANT'
        TECH = 'TECH'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(150), unique=True, nullable=False)
    firstname = db.Column(db.String(150), nullable=False)
    lastname = db.Column(db.String(150), nullable=False)
    email = db.Column(db.String(150), unique=True, nullable=False)
    phone_number = db.Column(db.String(50), nullable=False)
    role = db.Column(db.Enum(RoleEnum), nullable=False, default=RoleEnum.CONSULTANT)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    equipments = db.relationship('Equipment', secondary=user_equipment, back_populates='owners')
    user_equipments = db.relationship('Equipment', secondary=user_equipment, back_populates='users', overlaps='equipments')
    def save(self):
        db.session.add(self)
        db.session.commit()
    # New column for password
    password_hash = db.Column(db.String(128), nullable=False)

    # Setter and getter methods for password
    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    
class Equipment(BaseModel):
    __tablename__ = 'equipment'

    class StatusEnum(Enum):
        AVAILABLE = 'AVAILABLE'
        ASSIGNED = 'ASSIGNED'
        IN_MAINTENANCE = 'IN_MAINTENANCE'
        OUTDATED = 'OUTDATED'

    id = db.Column(db.Integer, primary_key=True)
    reference = db.Column(db.String(255), unique=True, nullable=False)
    type = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    brand = db.Column(db.String(255), nullable=False)
    status = db.Column(db.Enum(StatusEnum), nullable=False, default=StatusEnum.AVAILABLE)
    location = db.Column(db.String(255), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    owners = db.relationship('User', secondary=user_equipment, back_populates='equipments')
    owners = db.relationship('User', secondary=user_equipment, back_populates='equipments', overlaps='user_equipments')
    users = db.relationship('User', secondary=user_equipment, back_populates='user_equipments', overlaps='equipments')
    last_assignment_date = db.Column(db.DateTime)
    def serialize(self):
        return {
            'id': self.id,
            'reference': self.reference,
            'type': self.type,
            'brand': self.brand,
            'status': self.status.value,
            'location': self.location,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'affected_value' : 1 if self.status == Equipment.StatusEnum.ASSIGNED else 0,
            'last_assignment_date':self.last_assignment_date
        }
    
    def save(self, required_attributes=None):
        if required_attributes is None:
            required_attributes = []

        missing_attributes = [attr for attr in required_attributes if not getattr(self, attr, None)]
        if missing_attributes:
            error_message = f"The following required attributes are missing: {', '.join(missing_attributes)}"
            return {"error": error_message}

        db.session.add(self)
        db.session.commit()
        return {"success": "Equipment saved successfully"}



class Comment(BaseModel):
    __tablename__ = 'comment'

    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text, nullable=False)
    active = db.Column(db.Integer)
    parent_id = db.Column(db.Integer, db.ForeignKey(id))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def save(self):
        db.session.add(self)
        db.session.commit()

class Report(BaseModel):
    __tablename__ = 'report'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    file_name = db.Column(db.String(150))
    file_path = db.Column(db.String(150))
    active = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def save(self):
        db.session.add(self)
        db.session.commit()

# class EquipmentModelView(ModelView):
#     column_list = ['id', 'reference', 'type', 'brand', 'status', 'location', 'user_id']
#     form_extra_fields = {
#         'user_id': SelectField('User ID', choices=[], validators=[DataRequired()], widget=Select2Widget())
#     }

#     def on_form_prefill(self, form, id):
#         # Load the existing user IDs and populate the SelectField choices
#         users = User.query.all()
#         choices = [(str(user.id), str(user.id)) for user in users]
#         form.user_id.choices = choices

#     def create_form(self, obj=None):
#         form = super(EquipmentModelView, self).create_form(obj=obj)
#         self.on_form_prefill(form, None)
#         return form

#     form_args = {
#         'user_id': {
#             'form_kwargs': {
#                 'on_form_prefill': on_form_prefill
#             }
#         }
#     }
#     def on_model_change(self, form, model, is_created):
#         model.user_id = form.user_id.data
