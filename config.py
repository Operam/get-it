from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_migrate import Migrate
from dotenv import load_dotenv
from flask_swagger_ui import get_swaggerui_blueprint
from flask_cors import CORS
from flask_mail import Mail
load_dotenv() #load env variables from .env

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'verysecret'

db = SQLAlchemy(app)
admin=Admin(app, template_mode='bootstrap3')
migrate = Migrate(app, db)

SWAGGER_URL = "/swagger"
API_URL = "/static/swagger.json"
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL, API_URL, config={"app_name": "Seans-Python-Flask-REST-Geoprod"}
)

app.config['MAIL_SERVER'] = os.getenv('MAIL_SERVER')
app.config['MAIL_PORT'] = os.getenv('MAIL_PORT')
app.config['MAIL_USERNAME'] = os.getenv('MAIL_USERNAME')
app.config['MAIL_PASSWORD'] = os.getenv('MAIL_PASSWORD')
app.config['MAIL_USE_TLS'] = os.getenv('MAIL_USE_TLS') == 'True'
app.config['MAIL_USE_SSL'] = os.getenv('MAIL_USE_SSL') == 'True'
mail = Mail(app)

app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
from wtforms.validators import DataRequired

# class UserModelView(ModelView):
#     column_list = ['id', 'firstname', 'lastname', 'email', 'phone_number', 'location', 'created_at','updated_at']


def create_app():
    from models import User, Equipment, Comment, Report
    models = [User, Equipment, Comment, Report]
    for model in models:
        admin.add_view(ModelView(model, db.session))
    #admin.add_view(EquipmentModelView(Equipment, db.session))
    print("app created")
    return app



