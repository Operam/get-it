from flask import Blueprint,jsonify
from config import db
api = Blueprint("User API", __name__, url_prefix='/user')
from models import User , Equipment
from flask import Flask, request, jsonify
from utils import generate_token
from .user import UserRepository


@api.route('/add_users', methods=['POST'])
def create_users():
    # Get the data from the request body
    data = request.get_json()

    # Extract the list of user objects from the request
    users = data.get('users', [])

    # Create a list to hold the created user IDs
    user_ids = []

    # Iterate over the user objects and create User instances
    for user_data in users:
        # Extract the user data from the user object
        username = user_data.get('username')
        firstname = user_data.get('firstname')
        lastname = user_data.get('lastname')
        email = user_data.get('email')
        phone_number = user_data.get('phone_number')
        password = user_data.get('password')
        role = user_data.get('role')

        # Perform any necessary validation here

        # Create a new User instance
        user = User(
            username=username,
            firstname=firstname,
            lastname=lastname,
            email=email,
            phone_number=phone_number,
            password=password,
            role=role
        )

        # Save the user to the database
        try:
            user.save()
            user_ids.append(user.id)

            #send registration email to user
            email_sent = UserRepository.send_registration_email(user, password)
            if not email_sent:
                response = {'message': 'Failed to send registration email'}
                return jsonify(response), 500
        except Exception as e:
            response = {'message': 'Failed to create users', 'error': str(e)}
            return jsonify(response), 500

    response = {'message': 'Users created successfully', 'user_ids': user_ids}
    return jsonify(response), 201


@api.route('/get_users_with_equipments', methods=['GET'])
def get_users_with_equipment():
    users = User.query.all()
    result = []
    
    for user in users:
        user_data = {
            'id': user.id,
            'username': user.username,
            'firstname': user.firstname,
            'lastname': user.lastname,
            'email': user.email,
            'phone_number': user.phone_number,
            'role': user.role.value,
            'created_at': user.created_at,
            'updated_at': user.updated_at,
            'equipments': []
        }
        
        for equipment in user.equipments:
            equipment_data = {
                'id': equipment.id,
                'reference': equipment.reference,
                'type': equipment.type,
                'description': equipment.description,
                'brand': equipment.brand,
                'status': equipment.status.value,
                'location': equipment.location,
                'created_at': equipment.created_at,
                'updated_at': equipment.updated_at,
                'last_assignment_date': equipment.last_assignment_date
            }
            user_data['equipments'].append(equipment_data)
        
        result.append(user_data)
    
    return jsonify(result)

@api.route('/login', methods=['POST'])
def login():
    try:
        data = request.get_json()

        email = data.get('email')
        password = data.get('password')

        user = User.query.filter_by(email=email).first()
        if not user:
            return jsonify({'error': 'Invalid email'}), 401

        if not user.verify_password(password):
            return jsonify({'error': 'Invalid password'}), 401

        token = generate_token()
        user_data = {
            'id': user.id,
            'email': user.email,
            'username': user.username,
            'role': user.role.value,
            'firstname': user.firstname,
            'lastname': user.lastname,
            'phone_number': user.phone_number,
            'created_at': user.created_at,
            'updated_at': user.updated_at,
        }

        return jsonify({'token': token, 'user': user_data}), 200

    except Exception as e:
        return jsonify({'error': 'An error occurred: {}'.format(str(e))}), 400
    
@api.route('/change_password', methods=['POST'])
def change_password():
    try:
        data = request.get_json()

        email = data.get('email')
        current_password = data.get('current_password')
        new_password = data.get('new_password')

        user = User.query.filter_by(email=email).first()
        if not user:
            return jsonify({'error': 'Invalid email'}), 401

        if not user.verify_password(current_password):
            return jsonify({'error': 'Invalid current password'}), 401

        user.password = new_password
        user.save()

        return jsonify({'message': 'Password changed successfully'}), 200

    except Exception as e:
        return jsonify({'error': 'An error occurred: {}'.format(str(e))}), 400