#from config import mail
# import smtplib
# from email.mime.text import MIMEText
#import yagmail
import sendgrid
from sendgrid.helpers.mail import Mail
import os
from flask_mail import Message
from config import mail

class UserRepository:

    @staticmethod
    def send_registration_email(user, password):
        msg = Message(subject='Bienvenue sur notre site !', sender='gdeo.project@gmail.com', recipients=[user.email])
        msg.body = 'Bonjour {},\n\nVotre inscription a été réussie.\n\n\
            Voici vos informations de connexion :\n\nAdresse e-mail : {}\n\
            Mot de passe : {}\n\n\
            Merci de vous être inscrit.'.format(user.username, user.email, password)
        try:
            mail.send(msg)
            return True
        except Exception as e:
            return False
    



