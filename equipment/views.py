from flask import Blueprint, request, jsonify
from models import User, Equipment
from .equipment import EquipmentRepository
from config import db
from sqlalchemy import text
from datetime import datetime
api = Blueprint("Equipment API", __name__, url_prefix='/equipment')
from models import Equipment
from flask_login import login_required, current_user

@api.route('/add', methods=['POST']) 
def add_equipment():
    data = request.get_json()
    user_id = data.get('user_id') #TODO: à modifier après l'ajout de l'authentification
    user = User.query.get(user_id)

    if not user:
        return jsonify({'error': 'User not found'}), 404
    # Check if user is admin
    if user.role != User.RoleEnum.ADMIN:
        return jsonify({'error': 'User unauthorized'}), 401
    
    try:
        EquipmentRepository.add_equipment(data)
        return jsonify({'message': 'Equipment added successfully'}), 200
    
    except ValueError as ve:
        return jsonify({'error': str(ve)}), 401
    except Exception as e:
        db.session.rollback()
        # TODO: il faut séparer les exceptions normalement
        return jsonify({'error': 'An error occured'}), 401


@api.route('/assign_equipment', methods=['POST'])
def assign_equipment():
    data = request.get_json()
    assigner_id = data.get('assigner_id') #TODO: à modifier après l'ajout de l'authentification
    equipment_ref = data.get('equipment_ref')
    username = data.get('username')
    action = data.get('action')

    assigner = User.query.get(assigner_id) 
    user = User.query.filter_by(username=username).first()
    equipment = Equipment.query.filter_by(reference=equipment_ref).first()

    if not user or not equipment:
        return jsonify({'error': 'User or Equipment not found'}), 404

    if assigner.role not in [User.RoleEnum.ADMIN, User.RoleEnum.TECH]:
        return jsonify({'error': 'User unauthorized'}), 401

    if action not in [UserEquipment.ActionEnum.USE.value, UserEquipment.ActionEnum.MAINTENANCE.value]:
        return jsonify({'error': 'Invalid action'}), 400
    
    if EquipmentRepository.is_equipment_assigned(equipment.id):
        return jsonify({'error': 'Equipment already assigned'}), 400
    
    try:
        EquipmentRepository.assign_equipment(user.id,equipment.id,action)
        return jsonify({'message': 'Equipment assigned successfully'}), 200
    except ValueError as ve:
        return jsonify({'error': str(ve)}), 401
    except Exception as e:
        db.session.rollback()
        return jsonify({'error': 'An error occured'}), 401

@api.route('/user_equipments/<int:user_id>', methods=['GET'])
def get_user_equipments(user_id):
    user = User.query.get(user_id)
    if not user:
        return jsonify({'error': 'User not found'}), 404

    sql = """
    Select e.reference, e.type, e.brand, e.status, ue.assigned_at, ue.action, ue.returned_at
    from user_equipment ue
    join equipment e ON ue.equipment_id = e.id
    WHERE ue.user_id = :user_id
    """
    result = db.session.execute(text(sql), {'user_id': user_id}).fetchall()

    response = []
    for row in result:
        response.append({
            'reference': row.reference,
            'type': row.type,
            'brand': row.brand,
            'status': row.status,
            'assignedAt': row.assigned_at,
            'action' : row.action,
            'returnedAt': row.returned_at
        })

    return jsonify(response), 200


@api.route('/add_equipments', methods=['POST'])
def create_equipments():
    # Get the data from the request body
    data = request.get_json()

    # Extract the list of equipment objects from the request
    equipments = data.get('equipments', [])

    # Create a list to hold the created equipment IDs
    equipment_ids = []

    # Iterate over the equipment objects and create Equipment instances
    for equipment_data in equipments:
        # Extract the equipment data from the equipment object
        reference = equipment_data.get('reference')
        type = equipment_data.get('type')
        description = equipment_data.get('description')
        brand = equipment_data.get('brand')
        status = equipment_data.get('status')
        location = equipment_data.get('location')

        # Perform any necessary validation here

        # Create a new Equipment instance
        equipment = Equipment(
            reference=reference,
            type=type,
            description=description,
            brand=brand,
            status=status,
            location=location
        )

        # Save the equipment to the database
        try:
            equipment.save()
            equipment_ids.append(equipment.id)
        except Exception as e:
            response = {'message': 'Failed to create equipments', 'error': str(e)}
            return jsonify(response), 500

    response = {'message': 'Equipments created successfully', 'equipment_ids': equipment_ids}
    return jsonify(response), 201

#test
@api.route('/update_equipments/<equipment_id>', methods=['PUT'])
def update_equipment(equipment_id):
    # Get the equipment from the database
    equipment = Equipment.query.get_or_404(equipment_id)

    # Get the data from the request body
    data = request.get_json()

    # Update the equipment attributes
    equipment.reference = data.get('reference', equipment.reference)
    equipment.type = data.get('type', equipment.type)
    equipment.description = data.get('description', equipment.description)
    equipment.brand = data.get('brand', equipment.brand)
    equipment.status = data.get('status', equipment.status)
    equipment.location = data.get('location', equipment.location)
    
    # Save the updated equipment
    db.session.commit()

    # Return a JSON response
    return jsonify({'message': 'Equipment updated successfully'})

@api.route('/get_all_equipments', methods=['GET'])
def get_all_equipments():
    equipments = Equipment.query.all()
    equipment_list = [equipment.serialize() for equipment in equipments]
    return jsonify(equipment_list)
    
@api.route('/update_equipments', methods=['PUT'])
def update_equipments():
    """
        Update multiple equipments.
        ---
        tags:
          - Equipments
        parameters:
          - in: body
            name: equipments
            description: List of equipment data to update
            required: true
            schema:
              type: array
              items:
                type: object
                properties:
                  id:
                    type: integer
                    description: Equipment ID
                  reference:
                    type: string
                    description: Equipment reference
                  type:
                    type: string
                    description: Equipment type
                  brand:
                    type: string
                    description: Equipment brand
                  status:
                    type: string
                    description: Equipment status
                  location:
                    type: string
                    description: Equipment location
                  user_id:
                    type: integer
                    description: ID of the user associated with the equipment
        responses:
          200:
            description: Equipments updated successfully
          400:
            description: Error updating equipments
        """
    try:
        data = request.json

        for equipment_data in data:
            equipment_id = equipment_data['id']
            equipment = Equipment.query.get(equipment_id)

            if equipment:
                equipment.reference = equipment_data.get('reference', equipment.reference)
                equipment.type = equipment_data.get('type', equipment.type)
                equipment.brand = equipment_data.get('brand', equipment.brand)
                equipment.status = equipment_data.get('status', equipment.status)
                equipment.location = equipment_data.get('location', equipment.location)
                equipment.user_id = equipment_data.get('user_id', equipment.user_id)

        db.session.commit()

        return jsonify({'message': 'Equipments updated successfully'}), 200
    except Exception as e:
        db.session.rollback()
        return jsonify({'error': str(e)}), 400
    finally:
        db.session.close()

@api.route('/api/users/<user_id>/equipments/<equipment_id>', methods=['PUT'])
def assign_equipment_to_user(user_id, equipment_id):
    user = User.query.get(user_id)
    equipment = Equipment.query.get(equipment_id)

    if user is None or equipment is None:
        return jsonify({'error': 'User or equipment not found'}), 404

    # Assign the equipment to the user
    user.equipments.append(equipment)

    # Set the last_assignment_date to the current datetime
    equipment.last_assignment_date = datetime.now()
    equipment.status = Equipment.StatusEnum.ASSIGNED
    # Save the changes
    db.session.commit()

    return jsonify({'message': 'Equipment assigned successfully'})
