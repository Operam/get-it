from models import Equipment
from config import db
from error.exceptions import DuplicateReferenceException
from datetime import datetime


class EquipmentRepository(object):

    @staticmethod
    def add_equipment(data):
        #db.session.query(Equipment).delete()
        reference = EquipmentRepository.generate_reference()
        # existing_equipment = Equipment.query.filter_by(reference=reference).first()
        # if existing_equipment:
        #     raise DuplicateReferenceException(reference)
        equipment = Equipment(
        reference = reference,
        type = data.get('type'),
        brand = data.get('brand'),
        location = data.get('location'))
        
        #save
        required_attributes = {
            'reference': {'type': str, 'required': True},
            'type': {'type': str, 'required': True},
            'brand': {'type': str, 'required': True},
            'location': {'type': str, 'required': True}
        }
        equipment.save(required_attributes)

    @staticmethod
    def generate_reference():
        last_equipment = Equipment.query.order_by(Equipment.id.desc()).first()
        if last_equipment:
            last_reference = last_equipment.reference
            last_number = int(last_reference[1:])  #retrieve reference without letter E
            new_number = last_number + 1
            new_reference = 'E{:04d}'.format(new_number)
        else:
            new_reference = 'E0001' #first reference
        return new_reference

    @staticmethod
    def is_equipment_assigned(equipment_id):
        return UserEquipment.query.filter_by(equipment_id=equipment_id, returned_at=None).first() is not None
    
    
    @staticmethod
    def assign_equipment(user_id,equipment_id,action):
        assigned_at = datetime.utcnow()
        action_enum = {
            'use': UserEquipment.ActionEnum.USE,
            'maintenance': UserEquipment.ActionEnum.MAINTENANCE}
        
        action = action_enum[action]
        user_equipment = UserEquipment(user_id=user_id, equipment_id=equipment_id, action=action, assigned_at=assigned_at)
        
        #save
        required_attributes = {
            'user_id': {'type': int, 'required': True},
            'equipment_id': {'type': int, 'required': True},
            'action': {'type': UserEquipment.ActionEnum, 'required': True},
            'assigned_at': {'type': datetime, 'required': True}
        }
        user_equipment.save(required_attributes)
    
    