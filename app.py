#from flask import Flask
from comment.views import api as comment_api
from equipment.views import api as equipment_api
from report.views import api as report_api
from user.views import api as user_api
from config import create_app, db
import os
from dotenv import load_dotenv

load_dotenv()

app = create_app()
app.register_blueprint(comment_api)
app.register_blueprint(equipment_api)
app.register_blueprint(report_api)
app.register_blueprint(user_api)
if __name__ == "__main__":
    with app.app_context():
        db.create_all()
    
    app.config["DEBUG"] = True
    # app.config['SERVER_NAME'] = os.getenv('SERVER_NAME')
    # app.run(host='0.0.0.0')
    app.run(host='0.0.0.0',port=5005)